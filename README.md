# Zerven

Zerven is a Discord Bot for checking fuel prices and getting price alerts.

# Bugs

Time drifts for daily alerts if you don't request frequently. Drifts have been found with as little as 5 min intervals

As Zerven is designed for deployment on Heroku, some things may not work properly elsewhere. Please create an issue if you find anything like this.

## Screenshot
![screenshot1](https://i.imgur.com/P8TMiMJ.png)
![screenshot2](https://i.imgur.com/WQzmCZv.png)
![screenshot3](https://cdn.discordapp.com/attachments/949668996976869376/987388748092280842/unknown.png)

## License
Licensed under AGPL 3.0. Largely shares a codebase with [Consubot](https://github.com/epetousis/consubot)
