import { REST } from '@discordjs/rest';
import { Routes } from 'discord-api-types/v9';

import FuelCommands from './price';
import AlertCommands from './alerts';

const clientId = process.env.BOT_CLIENT_ID ?? '';
const devGuildId = process.env.BOT_DEV_GUILD_ID;
const token = process.env.BOT_AUTH_TOKEN ?? '';

const commands = [
  FuelCommands(),
  AlertCommands(),
].flat().map((command) => command.data.toJSON());

const rest = new REST({ version: '9' }).setToken(token);

const commandRoute = devGuildId
  ? Routes.applicationGuildCommands(clientId, devGuildId)
  : Routes.applicationCommands(clientId);

rest.put(commandRoute, { body: commands })
  .then(() => console.log('Application commands have been registered.'))
  .catch(console.error);
