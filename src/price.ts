import {
  ButtonInteraction,
  CommandInteraction,
  MessageEmbed,
  MessageActionRow,
  MessageButton,
  ColorResolvable,
} from 'discord.js';
import { SlashCommandBuilder } from '@discordjs/builders';
import axios from 'axios';

enum States {
  All = 'All',
  Vic = 'VIC',
  Nsw = 'NSW',
  Qld = 'QLD',
  Wa = 'WA',
  Act = 'ACT',
}

enum FuelType {
  E10 = 'E10',
  U91 = 'U91',
  U95 = 'U95',
  U98 = 'U98',
  Diesel = 'Diesel',
  LPG = 'LPG',
}

async function embedFuel(message: CommandInteraction, data: any, lng: number, lat: number) {
  const embedColours = ['#F4811F', '#008163', '#EE2526'];
  const randColour = Math.floor(Math.random() * 3);
  const embed = new MessageEmbed()
    .setTitle(`Cheapest prices for ${data.typeName} in ${data.regionName}`)
    .setFooter({ text: `Prices correct as at ${new Date(data.updateTime * 1000 + 36000000).toUTCString().toString().replace('GMT', 'AEST')}` })
    .setColor(embedColours[randColour] as ColorResolvable)
    .addFields([
      { name: data.pumps[0].name, value: `${data.pumps[0].price}`, inline: true },
      { name: data.pumps[1].name, value: `${data.pumps[1].price}`, inline: true },
      { name: data.pumps[2].name, value: `${data.pumps[2].price}`, inline: true },
    ]);
  const alertActionRow = new MessageActionRow()
    .addComponents(
      new MessageButton()
        .setCustomId(`coords|${lng}|${lat}|${data.pumps[0].name}`)
        .setLabel('Get coordinates')
        .setStyle('SECONDARY'),
    );
  await message.reply({ content: null, embeds: [embed], components: [alertActionRow] });
}

async function getFuel(interaction: CommandInteraction) {
  const state = interaction.options.getString('state') as States;
  const fType = interaction.options.getString('fueltype') as FuelType;
  const latestPrice = await axios({
    method: 'GET',
    url: 'https://projectzerothree.info/api.php?format=json',
  });
  switch (state) {
    case States.All:
      switch (fType) {
        case FuelType.E10: {
          const dataE10 = {
            regionName: latestPrice.data.regions[0].region,
            typeName: latestPrice.data.regions[0].prices[0].type,
            updateTime: latestPrice.data.updated,
            pumps: [
              {
                name: latestPrice.data.regions[0].prices[0].name,
                price: latestPrice.data.regions[0].prices[0].price,
              },
              {
                name: latestPrice.data.regions[5].prices[0].name,
                price: latestPrice.data.regions[5].prices[0].price,
              },
              {
                name: latestPrice.data.regions[10].prices[0].name,
                price: latestPrice.data.regions[10].prices[0].price,
              },
            ],
          };
          return embedFuel(
            interaction,
            dataE10,
            latestPrice.data.regions[0].prices[0].lng,
            latestPrice.data.regions[0].prices[0].lat,
          );
        }
        case FuelType.U91: {
          const data91 = {
            regionName: latestPrice.data.regions[0].region,
            typeName: latestPrice.data.regions[0].prices[1].type,
            updateTime: latestPrice.data.updated,
            pumps: [
              {
                name: latestPrice.data.regions[0].prices[1].name,
                price: latestPrice.data.regions[0].prices[1].price,
              },
              {
                name: latestPrice.data.regions[5].prices[1].name,
                price: latestPrice.data.regions[5].prices[1].price,
              },
              {
                name: latestPrice.data.regions[10].prices[1].name,
                price: latestPrice.data.regions[10].prices[1].price,
              },
            ],
          };
          return embedFuel(
            interaction,
            data91,
            latestPrice.data.regions[0].prices[1].lng,
            latestPrice.data.regions[0].prices[1].lat,
          );
        }
        case FuelType.U95: {
          const data95 = {
            regionName: latestPrice.data.regions[0].region,
            typeName: latestPrice.data.regions[0].prices[2].type,
            updateTime: latestPrice.data.updated,
            pumps: [
              {
                name: latestPrice.data.regions[0].prices[2].name,
                price: latestPrice.data.regions[0].prices[2].price,
              },
              {
                name: latestPrice.data.regions[5].prices[2].name,
                price: latestPrice.data.regions[5].prices[2].price,
              },
              {
                name: latestPrice.data.regions[10].prices[2].name,
                price: latestPrice.data.regions[10].prices[2].price,
              },
            ],
          };
          return embedFuel(
            interaction,
            data95,
            latestPrice.data.regions[0].prices[2].lng,
            latestPrice.data.regions[0].prices[2].lat,
          );
        }
        case FuelType.U98: {
          const data98 = {
            regionName: latestPrice.data.regions[0].region,
            typeName: latestPrice.data.regions[0].prices[3].type,
            updateTime: latestPrice.data.updated,
            pumps: [
              {
                name: latestPrice.data.regions[0].prices[3].name,
                price: latestPrice.data.regions[0].prices[3].price,
              },
              {
                name: latestPrice.data.regions[5].prices[3].name,
                price: latestPrice.data.regions[5].prices[3].price,
              },
              {
                name: latestPrice.data.regions[10].prices[3].name,
                price: latestPrice.data.regions[10].prices[3].price,
              },
            ],
          };
          return embedFuel(
            interaction,
            data98,
            latestPrice.data.regions[0].prices[3].lng,
            latestPrice.data.regions[0].prices[3].lat,
          );
        }
        case FuelType.Diesel: {
          const dataDiesel = {
            regionName: latestPrice.data.regions[0].region,
            typeName: latestPrice.data.regions[0].prices[4].type,
            updateTime: latestPrice.data.updated,
            pumps: [
              {
                name: latestPrice.data.regions[0].prices[4].name,
                price: latestPrice.data.regions[0].prices[4].price,
              },
              {
                name: latestPrice.data.regions[5].prices[4].name,
                price: latestPrice.data.regions[5].prices[4].price,
              },
              {
                name: latestPrice.data.regions[10].prices[4].name,
                price: latestPrice.data.regions[10].prices[4].price,
              },
            ],
          };
          return embedFuel(
            interaction,
            dataDiesel,
            latestPrice.data.regions[0].prices[4].lng,
            latestPrice.data.regions[0].prices[4].lat,
          );
        }
        case FuelType.LPG: {
          const dataLPG = {
            regionName: latestPrice.data.regions[0].region,
            typeName: latestPrice.data.regions[0].prices[5].type,
            updateTime: latestPrice.data.updated,
            pumps: [
              {
                name: latestPrice.data.regions[0].prices[5].name,
                price: latestPrice.data.regions[0].prices[5].price,
              },
              {
                name: latestPrice.data.regions[5].prices[5].name,
                price: latestPrice.data.regions[5].prices[5].price,
              },
              {
                name: latestPrice.data.regions[10].prices[5].name,
                price: latestPrice.data.regions[10].prices[5].price,
              },
            ],
          };
          return embedFuel(
            interaction,
            dataLPG,
            latestPrice.data.regions[0].prices[5].lng,
            latestPrice.data.regions[0].prices[5].lat,
          );
        }
        default:
          return interaction.reply('An error occured');
      }
    case States.Vic:
      switch (fType) {
        case FuelType.E10: {
          const dataE10 = {
            regionName: latestPrice.data.regions[1].region,
            typeName: latestPrice.data.regions[1].prices[0].type,
            updateTime: latestPrice.data.updated,
            pumps: [
              {
                name: latestPrice.data.regions[1].prices[0].name,
                price: latestPrice.data.regions[1].prices[0].price,
              },
              {
                name: latestPrice.data.regions[6].prices[0].name,
                price: latestPrice.data.regions[6].prices[0].price,
              },
              {
                name: latestPrice.data.regions[11].prices[0].name,
                price: latestPrice.data.regions[11].prices[0].price,
              },
            ],
          };
          return embedFuel(
            interaction,
            dataE10,
            latestPrice.data.regions[1].prices[0].lng,
            latestPrice.data.regions[1].prices[0].lat,
          );
        }
        case FuelType.U91: {
          const data91 = {
            regionName: latestPrice.data.regions[1].region,
            typeName: latestPrice.data.regions[1].prices[1].type,
            updateTime: latestPrice.data.updated,
            pumps: [
              {
                name: latestPrice.data.regions[1].prices[1].name,
                price: latestPrice.data.regions[1].prices[1].price,
              },
              {
                name: latestPrice.data.regions[6].prices[1].name,
                price: latestPrice.data.regions[6].prices[1].price,
              },
              {
                name: latestPrice.data.regions[11].prices[1].name,
                price: latestPrice.data.regions[11].prices[1].price,
              },
            ],
          };
          return embedFuel(
            interaction,
            data91,
            latestPrice.data.regions[1].prices[1].lng,
            latestPrice.data.regions[1].prices[1].lat,
          );
        }
        case FuelType.U95: {
          const data95 = {
            regionName: latestPrice.data.regions[1].region,
            typeName: latestPrice.data.regions[1].prices[2].type,
            updateTime: latestPrice.data.updated,
            pumps: [
              {
                name: latestPrice.data.regions[1].prices[2].name,
                price: latestPrice.data.regions[1].prices[2].price,
              },
              {
                name: latestPrice.data.regions[6].prices[2].name,
                price: latestPrice.data.regions[6].prices[2].price,
              },
              {
                name: latestPrice.data.regions[11].prices[2].name,
                price: latestPrice.data.regions[11].prices[2].price,
              },
            ],
          };
          return embedFuel(
            interaction,
            data95,
            latestPrice.data.regions[1].prices[2].lng,
            latestPrice.data.regions[1].prices[2].lat,
          );
        }
        case FuelType.U98: {
          const data98 = {
            regionName: latestPrice.data.regions[1].region,
            typeName: latestPrice.data.regions[1].prices[3].type,
            updateTime: latestPrice.data.updated,
            pumps: [
              {
                name: latestPrice.data.regions[1].prices[3].name,
                price: latestPrice.data.regions[1].prices[3].price,
              },
              {
                name: latestPrice.data.regions[6].prices[3].name,
                price: latestPrice.data.regions[6].prices[3].price,
              },
              {
                name: latestPrice.data.regions[11].prices[3].name,
                price: latestPrice.data.regions[11].prices[3].price,
              },
            ],
          };
          return embedFuel(
            interaction,
            data98,
            latestPrice.data.regions[1].prices[3].lng,
            latestPrice.data.regions[1].prices[3].lat,
          );
        }
        case FuelType.Diesel: {
          const dataDiesel = {
            regionName: latestPrice.data.regions[1].region,
            typeName: latestPrice.data.regions[1].prices[4].type,
            updateTime: latestPrice.data.updated,
            pumps: [
              {
                name: latestPrice.data.regions[1].prices[4].name,
                price: latestPrice.data.regions[1].prices[4].price,
              },
              {
                name: latestPrice.data.regions[6].prices[4].name,
                price: latestPrice.data.regions[6].prices[4].price,
              },
              {
                name: latestPrice.data.regions[11].prices[4].name,
                price: latestPrice.data.regions[11].prices[4].price,
              },
            ],
          };
          return embedFuel(
            interaction,
            dataDiesel,
            latestPrice.data.regions[1].prices[4].lng,
            latestPrice.data.regions[1].prices[4].lat,
          );
        }
        case FuelType.LPG: {
          const dataLPG = {
            regionName: latestPrice.data.regions[1].region,
            typeName: latestPrice.data.regions[1].prices[5].type,
            updateTime: latestPrice.data.updated,
            pumps: [
              {
                name: latestPrice.data.regions[1].prices[5].name,
                price: latestPrice.data.regions[1].prices[5].price,
              },
              {
                name: latestPrice.data.regions[6].prices[5].name,
                price: latestPrice.data.regions[6].prices[5].price,
              },
              {
                name: latestPrice.data.regions[11].prices[5].name,
                price: latestPrice.data.regions[11].prices[5].price,
              },
            ],
          };
          return embedFuel(
            interaction,
            dataLPG,
            latestPrice.data.regions[1].prices[5].lng,
            latestPrice.data.regions[1].prices[5].lat,
          );
        }
        default:
          return interaction.reply('An error occured');
      }
    case States.Nsw:
      // region 2 7 12
      switch (fType) {
        case FuelType.E10: {
          const dataE10 = {
            regionName: latestPrice.data.regions[2].region,
            typeName: latestPrice.data.regions[2].prices[0].type,
            updateTime: latestPrice.data.updated,
            pumps: [
              {
                name: latestPrice.data.regions[2].prices[0].name,
                price: latestPrice.data.regions[2].prices[0].price,
              },
              {
                name: latestPrice.data.regions[7].prices[0].name,
                price: latestPrice.data.regions[7].prices[0].price,
              },
              {
                name: latestPrice.data.regions[12].prices[0].name,
                price: latestPrice.data.regions[12].prices[0].price,
              },
            ],
          };
          return embedFuel(
            interaction,
            dataE10,
            latestPrice.data.regions[2].prices[0].lng,
            latestPrice.data.regions[2].prices[0].lat,
          );
        } case FuelType.U91: {
          const data91 = {
            regionName: latestPrice.data.regions[2].region,
            typeName: latestPrice.data.regions[2].prices[1].type,
            updateTime: latestPrice.data.updated,
            pumps: [
              {
                name: latestPrice.data.regions[2].prices[1].name,
                price: latestPrice.data.regions[2].prices[1].price,
              },
              {
                name: latestPrice.data.regions[7].prices[1].name,
                price: latestPrice.data.regions[7].prices[1].price,
              },
              {
                name: latestPrice.data.regions[12].prices[1].name,
                price: latestPrice.data.regions[12].prices[1].price,
              },
            ],
          };
          return embedFuel(
            interaction,
            data91,
            latestPrice.data.regions[2].prices[1].lng,
            latestPrice.data.regions[2].prices[1].lat,
          );
        }
        case FuelType.U95: {
          const data95 = {
            regionName: latestPrice.data.regions[2].region,
            typeName: latestPrice.data.regions[2].prices[2].type,
            updateTime: latestPrice.data.updated,
            pumps: [
              {
                name: latestPrice.data.regions[2].prices[2].name,
                price: latestPrice.data.regions[2].prices[2].price,
              },
              {
                name: latestPrice.data.regions[7].prices[2].name,
                price: latestPrice.data.regions[7].prices[2].price,
              },
              {
                name: latestPrice.data.regions[12].prices[2].name,
                price: latestPrice.data.regions[12].prices[2].price,
              },
            ],
          };
          return embedFuel(
            interaction,
            data95,
            latestPrice.data.regions[2].prices[2].lng,
            latestPrice.data.regions[2].prices[2].lat,
          );
        }
        case FuelType.U98: {
          const data98 = {
            regionName: latestPrice.data.regions[2].region,
            typeName: latestPrice.data.regions[2].prices[3].type,
            updateTime: latestPrice.data.updated,
            pumps: [
              {
                name: latestPrice.data.regions[2].prices[3].name,
                price: latestPrice.data.regions[2].prices[3].price,
              },
              {
                name: latestPrice.data.regions[7].prices[3].name,
                price: latestPrice.data.regions[7].prices[3].price,
              },
              {
                name: latestPrice.data.regions[12].prices[3].name,
                price: latestPrice.data.regions[12].prices[3].price,
              },
            ],
          };
          return embedFuel(
            interaction,
            data98,
            latestPrice.data.regions[2].prices[3].lng,
            latestPrice.data.regions[2].prices[3].lat,
          );
        }
        case FuelType.Diesel: {
          const dataDiesel = {
            regionName: latestPrice.data.regions[2].region,
            typeName: latestPrice.data.regions[2].prices[4].type,
            updateTime: latestPrice.data.updated,
            pumps: [
              {
                name: latestPrice.data.regions[2].prices[4].name,
                price: latestPrice.data.regions[2].prices[4].price,
              },
              {
                name: latestPrice.data.regions[7].prices[4].name,
                price: latestPrice.data.regions[7].prices[4].price,
              },
              {
                name: latestPrice.data.regions[12].prices[4].name,
                price: latestPrice.data.regions[12].prices[4].price,
              },
            ],
          };
          return embedFuel(
            interaction,
            dataDiesel,
            latestPrice.data.regions[2].prices[4].lng,
            latestPrice.data.regions[2].prices[4].lat,
          );
        }
        case FuelType.LPG: {
          const dataLPG = {
            regionName: latestPrice.data.regions[2].region,
            typeName: latestPrice.data.regions[2].prices[5].type,
            updateTime: latestPrice.data.updated,
            pumps: [
              {
                name: latestPrice.data.regions[2].prices[5].name,
                price: latestPrice.data.regions[2].prices[5].price,
              },
              {
                name: latestPrice.data.regions[7].prices[5].name,
                price: latestPrice.data.regions[7].prices[5].price,
              },
              {
                name: latestPrice.data.regions[12].prices[5].name,
                price: latestPrice.data.regions[12].prices[5].price,
              },
            ],
          };
          return embedFuel(
            interaction,
            dataLPG,
            latestPrice.data.regions[2].prices[5].lng,
            latestPrice.data.regions[2].prices[5].lat,
          );
        }
        default:
          return interaction.reply('An error occured');
      }
    case States.Qld:
      // region 3 8 13
      switch (fType) {
        case FuelType.E10: {
          const dataE10 = {
            regionName: latestPrice.data.regions[3].region,
            typeName: latestPrice.data.regions[3].prices[0].type,
            updateTime: latestPrice.data.updated,
            pumps: [
              {
                name: latestPrice.data.regions[3].prices[0].name,
                price: latestPrice.data.regions[3].prices[0].price,
              },
              {
                name: latestPrice.data.regions[8].prices[0].name,
                price: latestPrice.data.regions[8].prices[0].price,
              },
              {
                name: latestPrice.data.regions[13].prices[0].name,
                price: latestPrice.data.regions[13].prices[0].price,
              },
            ],
          };
          return embedFuel(
            interaction,
            dataE10,
            latestPrice.data.regions[3].prices[0].lng,
            latestPrice.data.regions[3].prices[0].lat,
          );
        } case FuelType.U91: {
          const data91 = {
            regionName: latestPrice.data.regions[3].region,
            typeName: latestPrice.data.regions[3].prices[1].type,
            updateTime: latestPrice.data.updated,
            pumps: [
              {
                name: latestPrice.data.regions[3].prices[1].name,
                price: latestPrice.data.regions[3].prices[1].price,
              },
              {
                name: latestPrice.data.regions[8].prices[1].name,
                price: latestPrice.data.regions[8].prices[1].price,
              },
              {
                name: latestPrice.data.regions[13].prices[1].name,
                price: latestPrice.data.regions[13].prices[1].price,
              },
            ],
          };
          return embedFuel(
            interaction,
            data91,
            latestPrice.data.regions[3].prices[1].lng,
            latestPrice.data.regions[3].prices[1].lat,
          );
        }
        case FuelType.U95: {
          const data95 = {
            regionName: latestPrice.data.regions[3].region,
            typeName: latestPrice.data.regions[3].prices[2].type,
            updateTime: latestPrice.data.updated,
            pumps: [
              {
                name: latestPrice.data.regions[3].prices[2].name,
                price: latestPrice.data.regions[3].prices[2].price,
              },
              {
                name: latestPrice.data.regions[8].prices[2].name,
                price: latestPrice.data.regions[8].prices[2].price,
              },
              {
                name: latestPrice.data.regions[13].prices[2].name,
                price: latestPrice.data.regions[13].prices[2].price,
              },
            ],
          };
          return embedFuel(
            interaction,
            data95,
            latestPrice.data.regions[3].prices[2].lng,
            latestPrice.data.regions[3].prices[2].lat,
          );
        }
        case FuelType.U98: {
          const data98 = {
            regionName: latestPrice.data.regions[3].region,
            typeName: latestPrice.data.regions[3].prices[3].type,
            updateTime: latestPrice.data.updated,
            pumps: [
              {
                name: latestPrice.data.regions[3].prices[3].name,
                price: latestPrice.data.regions[3].prices[3].price,
              },
              {
                name: latestPrice.data.regions[8].prices[3].name,
                price: latestPrice.data.regions[8].prices[3].price,
              },
              {
                name: latestPrice.data.regions[13].prices[3].name,
                price: latestPrice.data.regions[13].prices[3].price,
              },
            ],
          };
          return embedFuel(
            interaction,
            data98,
            latestPrice.data.regions[3].prices[3].lng,
            latestPrice.data.regions[3].prices[3].lat,
          );
        }
        case FuelType.Diesel: {
          const dataDiesel = {
            regionName: latestPrice.data.regions[3].region,
            typeName: latestPrice.data.regions[3].prices[4].type,
            updateTime: latestPrice.data.updated,
            pumps: [
              {
                name: latestPrice.data.regions[3].prices[4].name,
                price: latestPrice.data.regions[3].prices[4].price,
              },
              {
                name: latestPrice.data.regions[8].prices[4].name,
                price: latestPrice.data.regions[8].prices[4].price,
              },
              {
                name: latestPrice.data.regions[13].prices[4].name,
                price: latestPrice.data.regions[13].prices[4].price,
              },
            ],
          };
          return embedFuel(
            interaction,
            dataDiesel,
            latestPrice.data.regions[3].prices[4].lng,
            latestPrice.data.regions[3].prices[4].lat,
          );
        }
        case FuelType.LPG: {
          const dataLPG = {
            regionName: latestPrice.data.regions[3].region,
            typeName: latestPrice.data.regions[3].prices[5].type,
            updateTime: latestPrice.data.updated,
            pumps: [
              {
                name: latestPrice.data.regions[3].prices[5].name,
                price: latestPrice.data.regions[3].prices[5].price,
              },
              {
                name: latestPrice.data.regions[8].prices[5].name,
                price: latestPrice.data.regions[8].prices[5].price,
              },
              {
                name: latestPrice.data.regions[13].prices[5].name,
                price: latestPrice.data.regions[13].prices[5].price,
              },
            ],
          };
          return embedFuel(
            interaction,
            dataLPG,
            latestPrice.data.regions[3].prices[5].lng,
            latestPrice.data.regions[3].prices[5].lat,
          );
        }
        default:
          return interaction.reply('An error occured');
      }
    case States.Wa:
      // region 4 9 14
      switch (fType) {
        case FuelType.E10: {
          const dataE10 = {
            regionName: latestPrice.data.regions[4].region,
            typeName: latestPrice.data.regions[4].prices[0].type,
            updateTime: latestPrice.data.updated,
            pumps: [
              {
                name: latestPrice.data.regions[4].prices[0].name,
                price: latestPrice.data.regions[4].prices[0].price,
              },
              {
                name: latestPrice.data.regions[9].prices[0].name,
                price: latestPrice.data.regions[9].prices[0].price,
              },
              {
                name: latestPrice.data.regions[14].prices[0].name,
                price: latestPrice.data.regions[14].prices[0].price,
              },
            ],
          };
          return embedFuel(
            interaction,
            dataE10,
            latestPrice.data.regions[4].prices[0].lng,
            latestPrice.data.regions[4].prices[0].lat,
          );
        } case FuelType.U91: {
          const data91 = {
            regionName: latestPrice.data.regions[4].region,
            typeName: latestPrice.data.regions[4].prices[1].type,
            updateTime: latestPrice.data.updated,
            pumps: [
              {
                name: latestPrice.data.regions[4].prices[1].name,
                price: latestPrice.data.regions[4].prices[1].price,
              },
              {
                name: latestPrice.data.regions[5].prices[1].name,
                price: latestPrice.data.regions[9].prices[1].price,
              },
              {
                name: latestPrice.data.regions[14].prices[1].name,
                price: latestPrice.data.regions[14].prices[1].price,
              },
            ],
          };
          return embedFuel(
            interaction,
            data91,
            latestPrice.data.regions[4].prices[1].lng,
            latestPrice.data.regions[4].prices[1].lat,
          );
        }
        case FuelType.U95: {
          const data95 = {
            regionName: latestPrice.data.regions[4].region,
            typeName: latestPrice.data.regions[4].prices[2].type,
            updateTime: latestPrice.data.updated,
            pumps: [
              {
                name: latestPrice.data.regions[4].prices[2].name,
                price: latestPrice.data.regions[4].prices[2].price,
              },
              {
                name: latestPrice.data.regions[9].prices[2].name,
                price: latestPrice.data.regions[9].prices[2].price,
              },
              {
                name: latestPrice.data.regions[14].prices[2].name,
                price: latestPrice.data.regions[14].prices[2].price,
              },
            ],
          };
          return embedFuel(
            interaction,
            data95,
            latestPrice.data.regions[4].prices[2].lng,
            latestPrice.data.regions[4].prices[2].lat,
          );
        }
        case FuelType.U98: {
          const data98 = {
            regionName: latestPrice.data.regions[4].region,
            typeName: latestPrice.data.regions[4].prices[3].type,
            updateTime: latestPrice.data.updated,
            pumps: [
              {
                name: latestPrice.data.regions[4].prices[3].name,
                price: latestPrice.data.regions[4].prices[3].price,
              },
              {
                name: latestPrice.data.regions[9].prices[3].name,
                price: latestPrice.data.regions[9].prices[3].price,
              },
              {
                name: latestPrice.data.regions[14].prices[3].name,
                price: latestPrice.data.regions[14].prices[3].price,
              },
            ],
          };
          return embedFuel(
            interaction,
            data98,
            latestPrice.data.regions[4].prices[3].lng,
            latestPrice.data.regions[4].prices[3].lat,
          );
        }
        case FuelType.Diesel: {
          const dataDiesel = {
            regionName: latestPrice.data.regions[4].region,
            typeName: latestPrice.data.regions[4].prices[4].type,
            updateTime: latestPrice.data.updated,
            pumps: [
              {
                name: latestPrice.data.regions[4].prices[4].name,
                price: latestPrice.data.regions[4].prices[4].price,
              },
              {
                name: latestPrice.data.regions[9].prices[4].name,
                price: latestPrice.data.regions[9].prices[4].price,
              },
              {
                name: latestPrice.data.regions[14].prices[4].name,
                price: latestPrice.data.regions[14].prices[4].price,
              },
            ],
          };
          return embedFuel(
            interaction,
            dataDiesel,
            latestPrice.data.regions[4].prices[4].lng,
            latestPrice.data.regions[4].prices[4].lat,
          );
        }
        case FuelType.LPG: {
          const dataLPG = {
            regionName: latestPrice.data.regions[4].region,
            typeName: latestPrice.data.regions[4].prices[5].type,
            updateTime: latestPrice.data.updated,
            pumps: [
              {
                name: latestPrice.data.regions[4].prices[5].name,
                price: latestPrice.data.regions[4].prices[5].price,
              },
              {
                name: latestPrice.data.regions[9].prices[5].name,
                price: latestPrice.data.regions[9].prices[5].price,
              },
              {
                name: latestPrice.data.regions[14].prices[5].name,
                price: latestPrice.data.regions[14].prices[5].price,
              },
            ],
          };
          return embedFuel(
            interaction,
            dataLPG,
            latestPrice.data.regions[4].prices[5].lng,
            latestPrice.data.regions[4].prices[5].lat,
          );
        }
        default:
          return interaction.reply('An error occured');
      }
    case States.Act:
      // region 15 16 17
      switch (fType) {
        case FuelType.E10: {
          const dataE10 = {
            regionName: latestPrice.data.regions[15].region,
            typeName: latestPrice.data.regions[15].prices[0].type,
            updateTime: latestPrice.data.updated,
            pumps: [
              {
                name: latestPrice.data.regions[15].prices[0].name,
                price: latestPrice.data.regions[15].prices[0].price,
              },
              {
                name: latestPrice.data.regions[16].prices[0].name,
                price: latestPrice.data.regions[16].prices[0].price,
              },
              {
                name: latestPrice.data.regions[17].prices[0].name,
                price: latestPrice.data.regions[17].prices[0].price,
              },
            ],
          };
          return embedFuel(
            interaction,
            dataE10,
            latestPrice.data.regions[15].prices[0].lng,
            latestPrice.data.regions[15].prices[0].lat,
          );
        } case FuelType.U91: {
          const data91 = {
            regionName: latestPrice.data.regions[15].region,
            typeName: latestPrice.data.regions[15].prices[1].type,
            updateTime: latestPrice.data.updated,
            pumps: [
              {
                name: latestPrice.data.regions[15].prices[1].name,
                price: latestPrice.data.regions[15].prices[1].price,
              },
              {
                name: latestPrice.data.regions[16].prices[1].name,
                price: latestPrice.data.regions[16].prices[1].price,
              },
              {
                name: latestPrice.data.regions[17].prices[1].name,
                price: latestPrice.data.regions[17].prices[1].price,
              },
            ],
          };
          return embedFuel(
            interaction,
            data91,
            latestPrice.data.regions[15].prices[1].lng,
            latestPrice.data.regions[15].prices[1].lat,
          );
        }
        case FuelType.U95: {
          const data95 = {
            regionName: latestPrice.data.regions[15].region,
            typeName: latestPrice.data.regions[15].prices[2].type,
            updateTime: latestPrice.data.updated,
            pumps: [
              {
                name: latestPrice.data.regions[15].prices[2].name,
                price: latestPrice.data.regions[15].prices[2].price,
              },
              {
                name: latestPrice.data.regions[16].prices[2].name,
                price: latestPrice.data.regions[16].prices[2].price,
              },
              {
                name: latestPrice.data.regions[17].prices[2].name,
                price: latestPrice.data.regions[17].prices[2].price,
              },
            ],
          };
          return embedFuel(
            interaction,
            data95,
            latestPrice.data.regions[15].prices[2].lng,
            latestPrice.data.regions[15].prices[2].lat,
          );
        }
        case FuelType.U98: {
          const data98 = {
            regionName: latestPrice.data.regions[15].region,
            typeName: latestPrice.data.regions[15].prices[3].type,
            updateTime: latestPrice.data.updated,
            pumps: [
              {
                name: latestPrice.data.regions[15].prices[3].name,
                price: latestPrice.data.regions[15].prices[3].price,
              },
              {
                name: latestPrice.data.regions[16].prices[3].name,
                price: latestPrice.data.regions[16].prices[3].price,
              },
              {
                name: latestPrice.data.regions[17].prices[3].name,
                price: latestPrice.data.regions[17].prices[3].price,
              },
            ],
          };
          return embedFuel(
            interaction,
            data98,
            latestPrice.data.regions[15].prices[3].lng,
            latestPrice.data.regions[15].prices[3].lat,
          );
        }
        case FuelType.Diesel: {
          const dataDiesel = {
            regionName: latestPrice.data.regions[15].region,
            typeName: latestPrice.data.regions[15].prices[4].type,
            updateTime: latestPrice.data.updated,
            pumps: [
              {
                name: latestPrice.data.regions[15].prices[4].name,
                price: latestPrice.data.regions[15].prices[4].price,
              },
              {
                name: latestPrice.data.regions[16].prices[4].name,
                price: latestPrice.data.regions[16].prices[4].price,
              },
              {
                name: latestPrice.data.regions[17].prices[4].name,
                price: latestPrice.data.regions[17].prices[4].price,
              },
            ],
          };
          return embedFuel(
            interaction,
            dataDiesel,
            latestPrice.data.regions[15].prices[4].lng,
            latestPrice.data.regions[15].prices[4].lat,
          );
        }
        case FuelType.LPG: {
          const dataLPG = {
            regionName: latestPrice.data.regions[15].region,
            typeName: latestPrice.data.regions[15].prices[5].type,
            updateTime: latestPrice.data.updated,
            pumps: [
              {
                name: latestPrice.data.regions[15].prices[5].name,
                price: latestPrice.data.regions[15].prices[5].price,
              },
              {
                name: latestPrice.data.regions[16].prices[5].name,
                price: latestPrice.data.regions[16].prices[5].price,
              },
              {
                name: latestPrice.data.regions[17].prices[5].name,
                price: latestPrice.data.regions[17].prices[5].price,
              },
            ],
          };
          return embedFuel(
            interaction,
            dataLPG,
            latestPrice.data.regions[15].prices[5].lng,
            latestPrice.data.regions[15].prices[5].lat,
          );
        }
        default:
          return interaction.reply('An error occured');
      }
    default:
      return interaction.reply('An error occured');
  }
}

async function coords(interaction: ButtonInteraction) {
  const buttonIdByDelim = interaction.customId.split('|');
  const embedColours = ['#F4811F', '#008163', '#EE2526'];
  const randColour = Math.floor(Math.random() * 3);
  const embed = new MessageEmbed()
    .setTitle(`Coordinates for ${buttonIdByDelim[3]}`)
    .setColor(embedColours[randColour] as ColorResolvable)
    .addFields([
      { name: 'Latitude', value: `${buttonIdByDelim[2]}`, inline: true },
      { name: 'Longitude', value: `${buttonIdByDelim[1]}`, inline: true },
    ]);
  await interaction.reply({ content: null, embeds: [embed], ephemeral: true });
}

export function FuelButtons() {
  return [
    { handler: coords, id: 'coords' },
  ];
}

export default function FuelCommands() {
  return [
    {
      handler: getFuel,
      data: new SlashCommandBuilder()
        .setName('getfuel')
        .setDescription('Get the cheapest fuel prices')
        .addStringOption((option) => option.setName('state')
          .setDescription('State to check for cheapest prices')
          .setRequired(true)
          .addChoices(...Object.entries(States).map(([name, value]) => ({ name, value }))))
        .addStringOption((option) => option.setName('fueltype')
          .setDescription('Type of fuel to check for')
          .setRequired(true)
          .addChoices(...Object.entries(FuelType).map(([name, value]) => ({ name, value })))),
    },
  ];
}
