import {
  CommandInteraction,
  MessageEmbed,
  MessageActionRow,
  MessageSelectMenu,
  MessageButton,
  ColorResolvable,
  TextChannel,
  Client,
} from 'discord.js';
import { SlashCommandBuilder } from '@discordjs/builders';
import { createClient } from 'redis';
import axios from 'axios';
import selects from './menus';

enum States {
  All = 'All',
  Vic = 'VIC',
  Nsw = 'NSW',
  Qld = 'QLD',
  Wa = 'WA',
  Act = 'ACT',
}

enum FuelType {
  E10 = 'E10',
  U91 = 'U91',
  U95 = 'U95',
  U98 = 'U98',
  Diesel = 'Diesel',
  LPG = 'LPG',
}

enum ChannelSubs {
  Alerts = 'alerts',
  Daily = 'daily',
}

type Alert = {
  userID: string,
  type: FuelType,
  region: States,
  alertPrice: number,
  previousPrice: number
};

type ChannelAlert = {
  channelID: string,
  type: FuelType,
  region: States,
  alertPrice: number,
  previousPrice: number
};

type DailyData = {
  channelID: string,
  lastUpdate: number
};

const redisClient = createClient({ url: process.env.REDIS_URL });
redisClient.on('error', (err) => console.error('Redis Client Error', err));

async function alertFuel(interaction: CommandInteraction) {
  const state = interaction.options.getString('state') as States;
  const fType = interaction.options.getString('fueltype') as FuelType;
  const fPrice = interaction.options.getNumber('price') as number;
  const embedColours = ['#F4811F', '#008163', '#EE2526'];
  const randColour = Math.floor(Math.random() * 3);
  const embed = new MessageEmbed()
    .setTitle(`Alerts for ${fType}`)
    .setDescription(`Set alerts for ${fType} in ${state}!`)
    .setColor(embedColours[randColour] as ColorResolvable);
  const alertObject = {
    userID: interaction.user.id,
    type: fType,
    region: state,
    alertPrice: fPrice,
    previousPrice: 999.9,
  } as Alert;
  await redisClient.hSet('zerven:alerts:users', `${interaction.user.id}`, `${JSON.stringify(alertObject)}`);
  await interaction.reply({ content: null, embeds: [embed], ephemeral: true });
}

async function channelAlerts(interaction: CommandInteraction) {
  const commandType = interaction.options.getSubcommand() as ChannelSubs;
  switch (commandType) {
    case ChannelSubs.Alerts: {
      const state = interaction.options.getString('state') as States;
      const fType = interaction.options.getString('fueltype') as FuelType;
      const fPrice = interaction.options.getNumber('price') as number;
      const embedColours = ['#F4811F', '#008163', '#EE2526'];
      const randColour = Math.floor(Math.random() * 3);
      const embed = new MessageEmbed()
        .setTitle(`Alerts for ${fType}`)
        .setDescription(`Set alerts for ${fType} in <#${interaction.channelId}>!`)
        .setColor(embedColours[randColour] as ColorResolvable);
      const alertObject = {
        channelID: interaction.channelId,
        type: fType,
        region: state,
        alertPrice: fPrice,
        previousPrice: 999.9,
      } as ChannelAlert;
      await redisClient.hSet('zerven:alerts:channels', `${interaction.channelId}`, `${JSON.stringify(alertObject)}`);
      await interaction.reply({ content: null, embeds: [embed], ephemeral: true });
      break;
    }
    case ChannelSubs.Daily: {
      const embedColours = ['#F4811F', '#008163', '#EE2526'];
      const randColour = Math.floor(Math.random() * 3);
      const embed = new MessageEmbed()
        .setTitle('Alerts set-up!')
        .setDescription(`Set daily updates in <#${interaction.channelId}>!`)
        .setColor(embedColours[randColour] as ColorResolvable);
      const dailyObject = {
        channelID: interaction.channelId,
        lastUpdate: 0,
      } as DailyData;
      await redisClient.hSet('zerven:alerts:daily', `${interaction.channelId}`, `${JSON.stringify(dailyObject)}`);
      await interaction.reply({ content: null, embeds: [embed], ephemeral: true });
      break;
    }
    default:
      break;
  }
}

export default function AlertCommands() {
  return [
    {
      handler: alertFuel,
      data: new SlashCommandBuilder()
        .setName('alertme')
        .setDescription('Get alerts when the fuel drops below a certain price')
        .addStringOption((option) => option.setName('state')
          .setDescription('State to check for cheapest prices')
          .setRequired(true)
          .addChoices(...Object.entries(States).map(([name, value]) => ({ name, value }))))
        .addStringOption((option) => option.setName('fueltype')
          .setDescription('Type of fuel to check for')
          .setRequired(true)
          .addChoices(...Object.entries(FuelType).map(([name, value]) => ({ name, value }))))
        .addNumberOption((option) => option.setName('price')
          .setDescription('Price to alert when fuel is at or below (in cents)')
          .setMinValue(1)
          .setRequired(true)),
    }, {
      handler: channelAlerts,
      data: new SlashCommandBuilder()
        .setName('channelalerts')
        .setDescription('Sends alerts to this channel')
        .setDMPermission(false)
        .setDefaultMemberPermissions(8)
        .addSubcommand((subcommand) => subcommand.setName('alerts')
          .setDescription('Get alerts based on price in this channel')
          .addStringOption((option) => option.setName('state')
            .setDescription('State to check for cheapest prices')
            .setRequired(true)
            .addChoices(...Object.entries(States).map(([name, value]) => ({ name, value }))))
          .addStringOption((option) => option.setName('fueltype')
            .setDescription('Type of fuel to check for')
            .setRequired(true)
            .addChoices(...Object.entries(FuelType).map(([name, value]) => ({ name, value }))))
          .addNumberOption((option) => option.setName('price')
            .setDescription('Price to alert when fuel is at or below (in cents)')
            .setMinValue(1)
            .setRequired(true)))
        .addSubcommand((subcommand) => subcommand.setName('daily')
          .setDescription('Get daily alerts with fuel prices in this channel')),
    },
  ];
}

export function selectMenus() {
  return [
    { handler: selects, id: 'typemenu' },
    { handler: selects, id: 'regionmenu' },
  ];
}

export async function connectDB() {
  await redisClient.connect();
  console.log('Connected to redis');
}

export async function RunAlerts(client: Client<boolean>) {
  const latestPrice = await axios({
    method: 'GET',
    url: 'https://projectzerothree.info/api.php?format=json',
  });
  const userAlerts = await redisClient.hGetAll('zerven:alerts:users');
  Object.values(userAlerts).forEach(async (preParse: string) => {
    const val = JSON.parse(preParse) as Alert;
    let locInd = 0;
    let fuelInd = 0;
    const loc = val.region as States;
    const fType = val.type as FuelType;
    switch (loc) {
      case States.All: {
        locInd = 0;
        break;
      }
      case States.Vic: {
        locInd = 1;
        break;
      }
      case States.Nsw: {
        locInd = 2;
        break;
      }
      case States.Qld: {
        locInd = 3;
        break;
      }
      case States.Wa: {
        locInd = 4;
        break;
      }
      case States.Act: {
        locInd = 15;
        break;
      }
      default:
        break;
    }
    switch (fType) {
      case FuelType.E10: {
        fuelInd = 0;
        break;
      }
      case FuelType.U91: {
        fuelInd = 1;
        break;
      }
      case FuelType.U95: {
        fuelInd = 2;
        break;
      }
      case FuelType.U98: {
        fuelInd = 3;
        break;
      }
      case FuelType.Diesel: {
        fuelInd = 4;
        break;
      }
      case FuelType.LPG: {
        fuelInd = 5;
        break;
      }
      default:
        break;
    }
    const newPrice = latestPrice.data.regions[locInd].prices[fuelInd].price;
    if (newPrice < val.previousPrice && newPrice <= val.alertPrice) {
      const embedColours = ['#F4811F', '#008163', '#EE2526'];
      const randColour = Math.floor(Math.random() * 3);
      const embed = new MessageEmbed()
        .setTitle(`Price alert for ${val.type}!`)
        .setFooter({ text: `Correct as at ${new Date(latestPrice.data.updated * 1000 + 36000000).toUTCString().toString().replace('GMT', 'AEST')}` })
        .setColor(embedColours[randColour] as ColorResolvable)
        .addFields([
          { name: latestPrice.data.regions[locInd].prices[fuelInd].name, value: `${newPrice}`, inline: true },
          { name: 'Price difference', value: `${val.alertPrice - newPrice} below alert`, inline: true },
        ]);
      const alertActionRow = new MessageActionRow()
        .addComponents(
          new MessageButton()
            .setCustomId(`coords|${latestPrice.data.regions[locInd].prices[fuelInd].lng}|${latestPrice.data.regions[locInd].prices[fuelInd].lat}|${latestPrice.data.regions[locInd].prices[fuelInd].name}`)
            .setLabel('Get coordinates')
            .setStyle('SECONDARY'),
        );
      const alertUser = await client.users.fetch(val.userID);
      alertUser.send({ content: null, embeds: [embed], components: [alertActionRow] });
    }
    const alertObject = {
      userID: val.userID,
      type: fType,
      region: loc,
      alertPrice: val.alertPrice,
      previousPrice: newPrice,
    } as Alert;
    await redisClient.hSet('zerven:alerts:users', `${val.userID}`, `${JSON.stringify(alertObject)}`);
  });
  const channelsAlerts = await redisClient.hGetAll('zerven:alerts:channels');
  Object.values(channelsAlerts).forEach(async (preParse: string) => {
    const val = JSON.parse(preParse) as ChannelAlert;
    let locInd = 0;
    let fuelInd = 0;
    const loc = val.region as States;
    const fType = val.type as FuelType;
    switch (loc) {
      case States.All: {
        locInd = 0;
        break;
      }
      case States.Vic: {
        locInd = 1;
        break;
      }
      case States.Nsw: {
        locInd = 2;
        break;
      }
      case States.Qld: {
        locInd = 3;
        break;
      }
      case States.Wa: {
        locInd = 4;
        break;
      }
      case States.Act: {
        locInd = 15;
        break;
      }
      default:
        break;
    }
    switch (fType) {
      case FuelType.E10: {
        fuelInd = 0;
        break;
      }
      case FuelType.U91: {
        fuelInd = 1;
        break;
      }
      case FuelType.U95: {
        fuelInd = 2;
        break;
      }
      case FuelType.U98: {
        fuelInd = 3;
        break;
      }
      case FuelType.Diesel: {
        fuelInd = 4;
        break;
      }
      case FuelType.LPG: {
        fuelInd = 5;
        break;
      }
      default:
        break;
    }
    const newPrice = latestPrice.data.regions[locInd].prices[fuelInd].price;
    if (newPrice < val.previousPrice && newPrice <= val.alertPrice) {
      const embedColours = ['#F4811F', '#008163', '#EE2526'];
      const randColour = Math.floor(Math.random() * 3);
      const embed = new MessageEmbed()
        .setTitle(`Price alert for ${val.type}!`)
        .setFooter({ text: `Correct as at ${new Date(latestPrice.data.updated * 1000 + 36000000).toUTCString().toString().replace('GMT', 'AEST')}` })
        .setColor(embedColours[randColour] as ColorResolvable)
        .addFields([
          { name: latestPrice.data.regions[locInd].prices[fuelInd].name, value: `${newPrice}`, inline: true },
          { name: 'Price difference', value: `${val.alertPrice - newPrice} below alert`, inline: true },
        ]);
      const alertActionRow = new MessageActionRow()
        .addComponents(
          new MessageButton()
            .setCustomId(`coords|${latestPrice.data.regions[locInd].prices[fuelInd].lng}|${latestPrice.data.regions[locInd].prices[fuelInd].lat}|${latestPrice.data.regions[locInd].prices[fuelInd].name}`)
            .setLabel('Get coordinates')
            .setStyle('SECONDARY'),
        );
      const alertChannel = client.channels.cache.get(val.channelID) as TextChannel;
      alertChannel.send({ content: null, embeds: [embed], components: [alertActionRow] });
    }
    const alertObject = {
      channelID: val.channelID,
      type: fType,
      region: loc,
      alertPrice: val.alertPrice,
      previousPrice: newPrice,
    } as ChannelAlert;
    await redisClient.hSet('zerven:alerts:channels', `${val.channelID}`, `${JSON.stringify(alertObject)}`);
  });
  const dailyAlerts = await redisClient.hGetAll('zerven:alerts:daily');
  Object.values(dailyAlerts).forEach(async (preParse: string) => {
    const val = JSON.parse(preParse) as DailyData;
    if (Date.now() > val.lastUpdate && (Date.now() >= (val.lastUpdate + 86400000))) {
      const dailyObject = {
        channelID: val.channelID,
        lastUpdate: Date.now(),
      } as DailyData;
      await redisClient.hSet('zerven:alerts:daily', `${val.channelID}`, `${JSON.stringify(dailyObject)}`);
      const embedColours = ['#F4811F', '#008163', '#EE2526'];
      const randColour = Math.floor(Math.random() * 3);
      const embed = new MessageEmbed()
        .setTitle(`Daily fuel prices for ${latestPrice.data.regions[0].prices[0].type} in ${latestPrice.data.regions[0].region}`)
        .setFooter({ text: `Correct as at ${new Date(latestPrice.data.updated * 1000 + 36000000).toUTCString().toString().replace('GMT', 'AEST')}` })
        .setColor(embedColours[randColour] as ColorResolvable)
        .addFields([
          { name: latestPrice.data.regions[0].prices[0].name, value: `${latestPrice.data.regions[0].prices[0].price}`, inline: true },
          { name: latestPrice.data.regions[5].prices[0].name, value: `${latestPrice.data.regions[5].prices[0].price}`, inline: true },
          { name: latestPrice.data.regions[10].prices[0].name, value: `${latestPrice.data.regions[10].prices[0].price}`, inline: true },
        ]);
      const alertActionRow = new MessageActionRow()
        .addComponents(
          new MessageButton()
            .setCustomId(`coords|${latestPrice.data.regions[0].prices[0].lng}|${latestPrice.data.regions[0].prices[0].lat}|${latestPrice.data.regions[0].prices[0].name}`)
            .setLabel('Get coordinates')
            .setStyle('SECONDARY'),
        );
      const fuelActionRow = new MessageActionRow()
        .addComponents(
          new MessageSelectMenu()
            .setCustomId('typemenu')
            .addOptions([
              {
                label: 'E10',
                description: 'E10 fuel',
                value: '0',
                default: true,
              }, {
                label: 'U91',
                description: 'Unleaded 91 fuel',
                value: '1',
              }, {
                label: 'U95',
                description: 'Unleaded 95 fuel',
                value: '2',
              }, {
                label: 'U98',
                description: 'Unleaded 98 fuel',
                value: '3',
              }, {
                label: 'Diesel',
                description: 'Diesel fuel',
                value: '4',
              }, {
                label: 'LPG',
                description: 'LPG fuel',
                value: '5',
              },
            ]),
        );
      const regionActionRow = new MessageActionRow()
        .addComponents(
          new MessageSelectMenu()
            .setCustomId('regionmenu')
            .addOptions([
              {
                label: 'All',
                description: 'Get prices from all states',
                value: '0,5,10',
                default: true,
              }, {
                label: 'Victoria',
                description: 'Get prices from only Victoria',
                value: '1,6,11',
              }, {
                label: 'New South Wales',
                description: 'Get prices from only New South Wales',
                value: '2,7,12',
              }, {
                label: 'Queensland',
                description: 'Get prices from only Queensland',
                value: '3,8,13',
              }, {
                label: 'Western Australia',
                description: 'Get prices from only Western Australia',
                value: '4,9,14',
              }, {
                label: 'Australian Capital Territory',
                description: 'Get prices from only Australian Capital Territory',
                value: '15,16,17',
              },
            ]),
        );
      const dailyChannel = client.channels.cache.get(val.channelID) as TextChannel;
      dailyChannel.send({
        content: null,
        embeds: [embed],
        components: [alertActionRow, fuelActionRow, regionActionRow],
      });
    }
  });
}
