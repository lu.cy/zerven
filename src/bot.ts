import {
  Client,
  CommandInteraction,
  ButtonInteraction,
  SelectMenuInteraction,
  Intents,
  PresenceStatusData,
} from 'discord.js';
import {
  SlashCommandBuilder, SlashCommandSubcommandsOnlyBuilder,
} from '@discordjs/builders';
import express from 'express';

import FuelCommands, { FuelButtons } from './price';
import AlertCommands, { RunAlerts, selectMenus, connectDB } from './alerts';

type BotButton = {
  id: string,
  handler: (interaction: ButtonInteraction) => void | Promise<unknown>,
};

type BotSelectMenu = {
  id: string,
  handler: (interaction: SelectMenuInteraction) => void | Promise<unknown>,
};

type BotCommand = {
  data: Omit < SlashCommandBuilder,
  'addSubcommand' | 'addSubcommandGroup' > | SlashCommandSubcommandsOnlyBuilder,
  handler: (interaction: CommandInteraction) => void | Promise < unknown >,
};

const clientId = process.env.BOT_CLIENT_ID ?? '';
const permissions = 2147493888;
const token = process.env.BOT_AUTH_TOKEN ?? '';

const client = new Client({
  intents: [
    Intents.FLAGS.GUILDS,
    Intents.FLAGS.GUILD_MESSAGE_REACTIONS,
  ],
});

const buttonExports = [
  FuelButtons(),
].flat();

const buttonDefinitions = new Map<string, BotButton>();
buttonExports.forEach((button) => buttonDefinitions.set(button.id, button));

const selectExports = [
  selectMenus(),
].flat();

const selectDefinitions = new Map<string, BotSelectMenu>();
selectExports.forEach((select) => selectDefinitions.set(select.id, select));

const commandExports = [
  FuelCommands(),
  AlertCommands(),
].flat();

const commandDefinitions = new Map < string,
BotCommand >();
commandExports.forEach((command) => commandDefinitions.set(command.data.name, command));

client.once('ready', () => {
  connectDB();
  console.log('Ready!');

  if (!client.user) return;
  const randStatus = Math.floor(Math.random() * 3);
  const status = ['online' as PresenceStatusData, 'idle' as PresenceStatusData, 'dnd' as PresenceStatusData];
  client.user.setPresence({ activities: [{ name: 'fuel prices', type: 'WATCHING' }], status: status[randStatus] });
});

client.on('interactionCreate', async (interaction) => {
  if (interaction.isCommand()) {
    const commandDefinition = commandDefinitions.get(interaction.commandName);

    if (!commandDefinition) {
      await interaction.reply({ content: 'Command wasn\'t found. This can happen while the bot is in the middle of an update. Try again in a few minutes.', ephemeral: true });
      return;
    }

    try {
      await commandDefinition.handler(interaction);
      if (!interaction.replied) await interaction.reply({ content: 'Some goober added a command to me that didn\'t output a reply on completion. Possibly a bug?', ephemeral: true });
    } catch (error) {
      console.error('Error occurred during app command handling:', error);
      const messageContent = { content: 'There was an error while executing this command!', ephemeral: true };

      if (interaction.replied || interaction.deferred) {
        await interaction.followUp(messageContent);
      } else {
        await interaction.reply(messageContent);
      }
    }
  } else if (interaction.isButton()) {
    const buttonDefinition = buttonDefinitions.get(interaction.customId);

    if (!buttonDefinition) {
      const buttonIdByDelim = interaction.customId.split('|');
      if (buttonIdByDelim[0] === 'coords') {
        const coordButtonDefinition = buttonDefinitions.get(buttonIdByDelim[0]);
        if (!coordButtonDefinition) {
          await interaction.reply({ content: 'Button action failed to run. This can happen while the bot is in the middle of an update, or if the operator broke something. Try again in a few minutes.', ephemeral: true });
          return;
        }
        try {
          await coordButtonDefinition.handler(interaction);
        } catch (error) {
          console.error('Error occurred during button handling:', error);
          const messageContent = { content: 'There was an error while executing this button action!', ephemeral: true };

          if (interaction.replied || interaction.deferred) {
            await interaction.followUp(messageContent);
          } else {
            await interaction.reply(messageContent);
          }
        }
      } else {
        await interaction.reply({ content: 'Button action failed to run. This can happen while the bot is in the middle of an update, or if the operator broke something. Try again in a few minutes.', ephemeral: true });
      }
      return;
    }

    try {
      await buttonDefinition.handler(interaction);
    } catch (error) {
      console.error('Error occurred during button handling:', error);
      const messageContent = { content: 'There was an error while executing this button action!', ephemeral: true };

      if (interaction.replied || interaction.deferred) {
        await interaction.followUp(messageContent);
      } else {
        await interaction.reply(messageContent);
      }
    }
  } else if (interaction.isSelectMenu()) {
    const selectDefinition = selectDefinitions.get(interaction.customId);

    if (!selectDefinition) {
      await interaction.reply({ content: 'Button action failed to run. This can happen while the bot is in the middle of an update, or if the operator broke something. Try again in a few minutes.', ephemeral: true });
      return;
    }

    try {
      await selectDefinition.handler(interaction);
    } catch (error) {
      console.error('Error occurred during button handling:', error);
      const messageContent = { content: 'There was an error while executing this button action!', ephemeral: true };

      if (interaction.replied || interaction.deferred) {
        await interaction.followUp(messageContent);
      } else {
        await interaction.reply(messageContent);
      }
    }
  }
});

client.login(token);

const app = express();
const port = process.env.PORT || 4096;

app.use(express.static('public'));

app.get('/', (_req, res) => {
  RunAlerts(client);
  const randStatus = Math.floor(Math.random() * 3);
  const status = ['online' as PresenceStatusData, 'idle' as PresenceStatusData, 'dnd' as PresenceStatusData];
  if (client.user != null) {
    client.user.setPresence({ activities: [{ name: 'fuel prices', type: 'WATCHING' }], status: status[randStatus] });
  }
  res.header('Location', `https://discord.com/api/oauth2/authorize?client_id=${clientId}&scope=bot%20applications.commands&permissions=${permissions}`);
  res.status(302);
  res.send();
});

app.listen(port, () => {
  console.log(`Bot running on port ${port}`);
});
