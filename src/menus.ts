import {
  MessageEmbed,
  MessageActionRow,
  MessageSelectMenu,
  MessageButton,
  ColorResolvable,
  SelectMenuInteraction,
} from 'discord.js';
import axios from 'axios';

export default async function selects(interaction: SelectMenuInteraction) {
  const latestPrice = await axios({
    method: 'GET',
    url: 'https://projectzerothree.info/api.php?format=json',
  });
  let type = 0;
  let region = [0, 5, 10];
  const defaults = [[true, false, false, false, false, false],
    [true, false, false, false, false, false]];
  if (interaction.customId === 'typemenu') {
    defaults[1][0] = false;
    defaults[1][Number(interaction.values[0])] = true;
    type = Number(interaction.values[0]);
    interaction.message.components?.forEach((component) => {
      if (component.components[0].type === 'SELECT_MENU' && component.components[0].customId === 'regionmenu') {
        component.components[0].options.forEach((option) => {
          if (option.default) {
            defaults[0][0] = false;
            region = option.value.split(',').map((item) => Number(item));
            if (region[0] !== 15) {
              defaults[0][region[0]] = true;
            } else if (region[0] === 15) {
              defaults[0][5] = true;
            }
          }
        });
      }
    });
  }
  if (interaction.customId === 'regionmenu') {
    region = interaction.values[0].split(',').map((item) => Number(item));
    defaults[0][0] = false;
    if (region[0] !== 15) {
      defaults[0][region[0]] = true;
    } else if (region[0] === 15) {
      defaults[0][5] = true;
    }
    interaction.message.components?.forEach((component) => {
      if (component.components[0].type === 'SELECT_MENU' && component.components[0].customId === 'typemenu') {
        component.components[0].options.forEach((option) => {
          if (option.default) {
            type = Number(option.value);
            defaults[1][0] = false;
            defaults[1][Number(option.value)] = true;
          }
        });
      }
    });
  }
  const embedColours = ['#F4811F', '#008163', '#EE2526'];
  const randColour = Math.floor(Math.random() * 3);
  const embed = new MessageEmbed()
    .setTitle(`Daily fuel prices for ${latestPrice.data.regions[region[0]].prices[type].type} in ${latestPrice.data.regions[region[0]].region}`)
    .setFooter({ text: `Correct as at ${new Date(latestPrice.data.updated * 1000 + 36000000).toUTCString().toString().replace('GMT', 'AEST')}` })
    .setColor(embedColours[randColour] as ColorResolvable)
    .addFields([
      { name: latestPrice.data.regions[region[0]].prices[type].name, value: `${latestPrice.data.regions[region[0]].prices[type].price}`, inline: true },
      { name: latestPrice.data.regions[region[1]].prices[type].name, value: `${latestPrice.data.regions[region[1]].prices[type].price}`, inline: true },
      { name: latestPrice.data.regions[region[2]].prices[type].name, value: `${latestPrice.data.regions[region[2]].prices[type].price}`, inline: true },
    ]);
  const alertActionRow = new MessageActionRow()
    .addComponents(
      new MessageButton()
        .setCustomId(`coords|${latestPrice.data.regions[region[0]].prices[type].lng}|${latestPrice.data.regions[region[0]].prices[type].lat}|${latestPrice.data.regions[region[0]].prices[type].name}`)
        .setLabel('Get coordinates')
        .setStyle('SECONDARY'),
    );
  const fuelActionRow = new MessageActionRow()
    .addComponents(
      new MessageSelectMenu()
        .setCustomId('typemenu')
        .addOptions([
          {
            label: 'E10',
            description: 'E10 fuel',
            value: '0',
            default: defaults[1][0],
          }, {
            label: 'U91',
            description: 'Unleaded 91 fuel',
            value: '1',
            default: defaults[1][1],
          }, {
            label: 'U95',
            description: 'Unleaded 95 fuel',
            value: '2',
            default: defaults[1][2],
          }, {
            label: 'U98',
            description: 'Unleaded 98 fuel',
            value: '3',
            default: defaults[1][3],
          }, {
            label: 'Diesel',
            description: 'Diesel fuel',
            value: '4',
            default: defaults[1][4],
          }, {
            label: 'LPG',
            description: 'LPG fuel',
            value: '5',
            default: defaults[1][5],
          },
        ]),
    );
  const regionActionRow = new MessageActionRow()
    .addComponents(
      new MessageSelectMenu()
        .setCustomId('regionmenu')
        .addOptions([
          {
            label: 'All',
            description: 'Get prices from all states',
            value: '0,5,10',
            default: defaults[0][0],
          }, {
            label: 'Victoria',
            description: 'Get prices from only Victoria',
            value: '1,6,11',
            default: defaults[0][1],
          }, {
            label: 'New South Wales',
            description: 'Get prices from only New South Wales',
            value: '2,7,12',
            default: defaults[0][2],
          }, {
            label: 'Queensland',
            description: 'Get prices from only Queensland',
            value: '3,8,13',
            default: defaults[0][3],
          }, {
            label: 'Western Australia',
            description: 'Get prices from only Western Australia',
            value: '4,9,14',
            default: defaults[0][4],
          }, {
            label: 'Australian Capital Territory',
            description: 'Get prices from only Australian Capital Territory',
            value: '15,16,17',
            default: defaults[0][5],
          },
        ]),
    );
  interaction.update({
    content: null,
    embeds: [embed],
    components: [alertActionRow, fuelActionRow, regionActionRow],
  });
}
